import os
import json
import unittest
from bson.json_util import loads, dumps

from helpers.stream import encoder
from server.server import server



class TestSongController(unittest.TestCase):
    def setUp(self):
        server.config['TESTING'] = True
        server.config['DEBUG'] = False
        self.server = server.test_client()

        self.assertEqual(server.debug, False)

    def song_provvider_good(self):
        return {'name': 'sample', 'content': open('assets/sample.mp3', 'rb')}

    def test_song_api_response_get(self):
        response = self.server.get('/songs')
        self.assertEqual(response.status_code, 200)