import os
import unittest

from server.server import server

class TestHome(unittest.TestCase):
    def setUp(self):
        server.config['TESTING'] = True
        server.config['DEBUG'] = False
        self.server = server.test_client()
        self.assertEqual(server.debug, False)

    def tearDown(self):
        pass

    def test_root_status_get(self):
        response = self.server.get('/')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json['message'], 'welcome to sound api')
    def test_root_status_post(self):
        response = self.server.post('/')
        self.assertEqual(response.status_code, 405)
        self.assertEqual(response.json['message'], 'The method is not allowed for the requested URL.')
