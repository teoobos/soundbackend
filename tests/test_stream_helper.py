import os
import unittest
from io import BytesIO
#from server.server import server
from helpers.stream import encoder, decoder

class TestStreamHelper(unittest.TestCase):

    def test_streamer(self):
        song = open('assets/sample.mp3', 'rb')
        
        encoded = encoder(song)
        decoded = decoder(encoded)

        self.assertEqual(0, 0)
