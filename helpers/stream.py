from base64 import b64encode, b64decode

def streamer(song):
    """
    with open('assets/sample.mp3', 'rb') as mp3:
        data = mp3.read(1024)
        while data:
            yield data
            data = mp3.read(1024)
    """
    check = song.read(1204)
    while check:
        yield check
        check = song.read(1204)

def encoder(audio):
    return b64encode(audio.read())

def decoder(encoded):
    return b64decode(encoded)
