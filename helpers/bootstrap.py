class BootStrap:
    def __init__(self, server, api, db):
        self.server = server
        self.api = api
        self.db = db

    def getServer(self):
        return self.server

    def getApi(self):
        return self.api

    def getDb(self):
        return self.db