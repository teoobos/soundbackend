from base64 import b64decode, b64encode
import os


class FileSystemActions:
    def __init__(self, workdir='storage/'):
        self.workdir = workdir

    def save(self, name, content):
        with open(self.workdir + name, 'wb') as mp3:
            mp3.write(self.encoder(content))

    def stream(self, content):

        song = content.read(1024)
        while song:
            yield song
            song = content.read(1024)

    def encoder(self, string):
        return b64encode(string.encode('utf-8')).decode('utf-8')

    def decoder(self, encoded):
        return b64decode(encoded).decode('utf-8')
