from flask import Flask
from flask_restful import Resource, Api
from flask_cors import CORS
#from flask_sqlalchemy import SQLAlchemy
from helpers.mongojson import MongoJSONEncoder, ObjectIdConverter
from server.controllers.HomeController import HomeController
from server.controllers.SongController import SongController
from server.controllers.SongListController import SongListController

server = Flask(__name__)
CORS(server)
api = Api(server)

server.config['MAX_CONTENT_LENGTH'] = 1024 * 1024 * 1024

api.add_resource(HomeController, '/')
api.add_resource(SongListController, '/songs')
api.add_resource(SongController, '/songs/<string:song>')
    
