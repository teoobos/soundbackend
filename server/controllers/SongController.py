from flask import Response, request, jsonify
from flask_restful import Resource, reqparse
from helpers.stream import streamer
from helpers.mongojson import ObjectIdConverter
from helpers.filesystemactions import FileSystemActions
from helpers.stream import encoder
from bson.json_util import dumps
import werkzeug
import tarfile
import os
from io import BytesIO


class SongController(Resource, FileSystemActions):
    def __init__(self):
        super().__init__()

    def get(self, song):
        with tarfile.open('./assets/chillwave.tar.gz', 'r:gz') as songs:
            song = songs.extractfile(self.decoder(song.replace('ù', '/'))).read()
            return Response(song, mimetype='audio/mpeg')
