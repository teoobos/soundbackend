from flask import Response, request, jsonify
from flask_restful import Resource, reqparse
from helpers.stream import streamer, encoder
from helpers.filesystemactions import FileSystemActions
from server.database import db_session
from helpers.stream import encoder
from bson.json_util import dumps
import werkzeug
import tarfile
import os


class SongListController(Resource, FileSystemActions):
    def __init__(self):
        super().__init__()
    
    def get(self):
        with tarfile.open('./assets/chillwave.tar.gz', 'r:gz') as songs:
            return [
                {
                    'id': self.encoder(el).replace('/', 'ù') , 
                    'song':  el.replace('cutted/', '').replace('.mp3', '').split(' - ')[1], 
                    'artist': el.replace('cutted/', '').replace('.mp3', '').split(' - ')[0], 
                    'name': el
                }  for el in songs.getnames() if el != "cutted"
            ]
