import os
import requests

directory = '/home/teo/Music/mp3/ACDC/BACK_IN_BLACK/'

def puttin(filename):
    args = {'name': filename}
    files = {'content': open(directory + filename, 'rb')}
    try:
        response = requests.post('http://localhost:5000/songs', data=args, files=files, stream=True).status_code
    except Exception as e:
        response = 500
    return response

songs = [puttin(f) for f in os.listdir(directory)[:10]]

print(songs)

