# sound-backend

[![pipeline status](https://gitlab.com/teoobos/soundbackend/badges/master/pipeline.svg)](https://gitlab.com/teoobos/soundbackend/commits/master)
[![coverage report](https://gitlab.com/teoobos/soundbackend/badges/master/coverage.svg)](https://gitlab.com/teoobos/soundbackend/commits/master)

start environment

``` bash
python3 -m venv venv
. venv/bin/activate
```

run tests

``` bash
coverage run -m nose2
coverage report -m
```

run application

``` bash
python wsgi.py
```
``` bash
pylint ./**/*.py
```
``` bash
autopep8 **/**/*.py --in-place
```